import Entity from './Entity.js';

export default class StarWarsUniverse {
    constructor() {
        this.entities = [];
    }

    async init() {
        const API_URL = 'https://swapi.boom.dev/api/';

        const response = await fetch(API_URL).then(r => r.json())
        for (const [key, value] of Object.entries(response)) {

            const data = await fetch(value).then(d => d.json());
            const entity = new Entity(key, data);
            this.entities.push(entity);
        };
    }
}